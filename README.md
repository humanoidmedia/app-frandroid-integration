# Vues HTML pour l'application mobile FrAndroid

### Tech
* HTML
* Sass (Compass)
* JS

### Structure
* images (sprites)
* sass
* stylesheets (css compilés)
* scripts (js)

### Workflow

Pour l'intération d'un nouvelle vue, il faut se baser sur un contenu publié sur frandroid.com. En fonction du type de contenu à intégrer (simple article, dossier, bloc application bloc produit etc), je cherche un post et je recupère son ID afin d'avoir le json qui lui est associé :

http://direct.frandroid.com/?json=get_post&custom_fields=_post_format,dsq_thread_id,published_on_id,num_pages&id=ID_A_REMPLACER

Je créé ensuite un nouveau fichier html.
Les principales modifications CSS ont lieu dans le bloc :

```css
.post-content {
    .mon-nouveau-bloc {
        ...
    }
}
```

... tout en executant :

```sh
$ compass watch
```

### Livraison

La première chose à faire et de compiler le css mais sans les commentaires, les tabulations etc... Il suffit d'utiliser le fichier prod_config.rb prévu :

```sh
$ compass compile -c prod_config.rb --force
```

Lors d'une livraison pour Sidero, il faut faire un zip des dossiers/fichiers suivant :

* images
* stylesheets (css compilés)
* scripts (js)
* webview.html (optionnel, seulement dans le cas ou la structure html a changé)