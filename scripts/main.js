// Android comments handler
function showComments() {
    Android.showComments();
}

function editDomReview() {
    // In each criteria, swap position of grade node with description node
    var criterias = document.getElementsByClassName('device-rating');
    for (var i = 0; i < criterias.length; i++) {
        var criteriaNodes = criterias[i].children;
        var gradeNode = criteriaNodes[2];
        var descriptionNode = criteriaNodes[1];
        gradeNode.parentNode.insertBefore(gradeNode, descriptionNode);
    }

    // Ajouter un titre pour les deux partie du good-bad-container
    var goodBadContainer = document.getElementsByClassName('good-bad-container');
    var goodBadContainerChildren = goodBadContainer[0].children;

    var goodPart = goodBadContainerChildren[0];
    var newGoodElement = '<p class="goodBadTitle"><span class="picto good"></span>Pour</p>';
    goodPart.innerHTML = newGoodElement + goodPart.innerHTML;

    var badPart = goodBadContainerChildren[2];
    var newBadElement = '<p class="goodBadTitle"><span class="picto bad"></span>Contre</p>';
    badPart.innerHTML = newBadElement + badPart.innerHTML;
}

editDomReview();
